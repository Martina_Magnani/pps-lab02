package u02lab.code;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public abstract class AbstractGenerator implements SequenceGenerator {

    private final int start;
    private final int stop;
    private int currentPosition;

    protected AbstractGenerator (int start, int stop) {
        this.start = start;
        this.stop = stop;
        this.currentPosition = this.start;
    }

    @Override
    public Optional<Integer> next() {
        if (!isOver()) {
            return Optional.of(getNextValue());
        }
        return Optional.empty();
    }

    @Override
    public void reset() {
        this.currentPosition = this.start;
    }

    @Override
    public boolean isOver() {
        return this.currentPosition > this.stop;
    }

    @Override
    public List<Integer> allRemaining() {
        List<Integer> remainingElements = new ArrayList<>();
        while (currentPosition <= this.stop) {
            remainingElements.add(getNextValue());
        }
        return Collections.unmodifiableList(remainingElements);
    }

    protected abstract int nextValue(int currentPosition);

    private int getNextValue() {
        int nextValue = this.nextValue(currentPosition);
        this.currentPosition++;
        return nextValue;
    }
}
