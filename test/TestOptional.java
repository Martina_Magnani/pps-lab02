import org.junit.Assert;
import org.junit.Test;

import java.util.NoSuchElementException;
import java.util.Optional;

public class TestOptional {

    @Test public void testOptionalValue() {
        Optional<Integer> optionalInteger = Optional.of(10);
        Assert.assertTrue(optionalInteger.isPresent());
    }

    @Test public void testNonExistingOptionalValue() {
        Optional<Integer> optionalInteger = Optional.empty();
        Assert.assertFalse(optionalInteger.isPresent());
    }

    @Test public void testGetOptionalValue() {
        Optional<Integer> optionalInteger = Optional.of(10);
        Assert.assertEquals(new Integer(10), optionalInteger.get());
    }

    @Test public void testGetOrElseOptionalValue() {
        Optional<Integer> optionalInteger = Optional.empty();
        Assert.assertEquals(new Integer(7), optionalInteger.orElse(7));

        optionalInteger = Optional.of(4);
        Assert.assertEquals(new Integer(4), optionalInteger.orElse(7));
    }

    @Test (expected = NoSuchElementException.class)
    public void testGetFromOptional() {
        Optional<Integer> optionalInteger = Optional.empty();
        optionalInteger.get();
    }
}
