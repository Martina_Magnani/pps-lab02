package u02lab.code;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class RandomGeneratorTest {
    private final static int STOP = 10;
    private final static int MIDDLE = 5;
    private RandomGenerator generator;

    @Before public void setUp() throws Exception {
        this.generator = new RandomGenerator(this.STOP);
    }

    @Test public void testProducedNRandomElements() {
        for (int i=0; i<=this.STOP; i++) {
            Assert.assertTrue(this.generator.next().isPresent());
        }
        Assert.assertFalse(this.generator.next().isPresent());
    }

    @Test public void testReset() {
        for (int i=0; i<=this.STOP; i++) {
            this.generator.next();
        }
        this.generator.reset();
        Assert.assertTrue(this.generator.next().isPresent());
    }

    @Test public void testIsOver() {
        for (int i=0; i<=this.STOP; i++) {
            Assert.assertFalse(this.generator.isOver());
            this.generator.next();
        }
        Assert.assertTrue(this.generator.isOver());
    }

    @Test public void testRemainingElements() {
        for (int i=0; i<=this.MIDDLE; i++) {
            this.generator.next();
        }
        Assert.assertEquals(5, this.generator.allRemaining().size());
    }
}