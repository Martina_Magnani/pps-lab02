package u02lab.code;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.ranges.Range;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class RangeGeneratorTest {

    private final static int START = 5;
    private final static int STOP = 10;
    private final static int MIDDLE = 7;
    private RangeGenerator generator;

    @Before public void setUp() throws Exception {
        this.generator = new RangeGenerator(5,10);
    }

    @Test public void testProducedElementsFromStartToStop() {
        for (int i=this.START; i<=this.STOP; i++) {
            Assert.assertEquals(new Integer(i), this.generator.next().get());
        }
    }

    @Test public void testCallingNextAfterStop() {
        for (int i=this.START; i<=this.STOP; i++) {
            this.generator.next();
        }
        Assert.assertFalse(this.generator.next().isPresent());
    }

    @Test public void testReset() {
        for (int i=this.START; i<=this.MIDDLE; i++) {
            this.generator.next();
        }
        this.generator.reset();
        Assert.assertEquals(new Integer(this.START), this.generator.next().get());
    }

    @Test public void testIsOver() {
        for (int i=this.START; i<=this.MIDDLE; i++) {
            this.generator.next();
        }
        Assert.assertFalse(this.generator.isOver());
        for (int i=this.MIDDLE+1; i<=this.STOP; i++) {
            this.generator.next();
        }
        Assert.assertTrue(this.generator.isOver());
    }

    @Test public void testRemainingElements() {
        List<Integer> listItems = new ArrayList<>();
        for (int i=this.START; i<=this.STOP; i++) {
            listItems.add(i);
        }
        Assert.assertEquals(listItems, generator.allRemaining());
    }
}